//
//  ILoginService.h
//  ThunderDelivery
//
//  Created by david delgado on 9/3/15.
//  Copyright (c) 2015 david delgado. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ILoginService <NSObject>
-(BOOL)doLoginWith:(NSString*)username:(NSString*)password;
@end
