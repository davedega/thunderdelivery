//
//  LoginServiceImpl.h
//  ThunderDelivery
//
//  Created by david delgado on 9/3/15.
//  Copyright (c) 2015 david delgado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ILoginService.h"
@interface LoginServiceImpl : NSObject <ILoginService>

@end
