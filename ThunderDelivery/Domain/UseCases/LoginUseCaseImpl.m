//
//  LoginUseCaseImpl.m
//  ThunderDelivery
//
//  Created by david delgado on 9/10/15.
//  Copyright (c) 2015 david delgado. All rights reserved.
//

#import "LoginUseCaseImpl.h"

@implementation LoginUseCaseImpl

@synthesize loginService;

-(BOOL)loginWith:(NSString*)username:(NSString*)andPassword{
    return [loginService doLoginWith:username :andPassword];
}
@end
