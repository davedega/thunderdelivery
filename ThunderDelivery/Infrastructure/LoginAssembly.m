//
//  LoginAssembly.m
//  ThunderDelivery
//
//  Created by david delgado on 9/11/15.
//  Copyright (c) 2015 david delgado. All rights reserved.
//

#import "LoginAssembly.h"
#import "LoginServiceImpl.h"

@implementation LoginAssembly
#pragma mark - bind de LoginViewController con ILoginUseCase
- (LoginViewController *)loginViewController{
    return [TyphoonDefinition withClass:[LoginViewController class] configuration:^(TyphoonDefinition *definition)
            
            {
                
                [definition injectProperty:@selector(loginUseCase) with:[self loginUseCase]];
                
            }];
}
- (id<ILoginUseCase>)loginUseCase{
    return [TyphoonDefinition withClass:[LoginUseCaseImpl class] configuration:^(TyphoonDefinition *definition)
            
            {
                
                [definition injectProperty:@selector(loginService) with:[self loginService]];
                
            }];
}

#pragma mark - bind de LoginUseCaseImpl con ILoginService

- (id<ILoginService>)loginService{
    return [TyphoonDefinition withClass:[LoginServiceImpl class]];
}
@end
