//
//  LoginViewController.m
//  ThunderDelivery
//
//  Created by david delgado on 9/1/15.
//  Copyright (c) 2015 david delgado. All rights reserved.
//

#import "LoginViewController.h"
#import "ILoginViewModel.h"
#import "ILoginService.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

@synthesize loginUseCase;

id<ILoginViewModel> viewModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    viewModel = (id<ILoginViewModel>)self.view;
    [viewModel setListener:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) doLogin{
    
    NSLog(@"Controller with user: %@",[viewModel getUsername]);
    NSLog(@"Controller with password: %@",[viewModel getPassword]);
    
    if( [loginUseCase loginWith:[viewModel getUsername]:[viewModel getPassword]]){
        [viewModel showWelcomeMessage];
    }else{
        [viewModel showBadAccesMessage];

    }

}


@end
