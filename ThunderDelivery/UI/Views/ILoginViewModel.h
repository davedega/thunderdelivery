//
//  ILoginViewModel.h
//  ThunderDelivery
//
//  Created by david delgado on 9/2/15.
//  Copyright (c) 2015 david delgado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ILoginViewModelListener.h"


@protocol ILoginViewModel <NSObject>

//obtiene el nombre de usuario desde la UI
-(NSString*) getUsername;

//obtiene el password desde UI
-(NSString*) getPassword;

// Se muestra cuando el login es correcto
-(void)showWelcomeMessage;

// Se muestra cuando el login es incorrecto
-(void)showBadAccesMessage;

//setea el listener para las acciones
-(void)setListener:(id<ILoginViewModelListener>)listener;

@end
