//
//  LoginView.h
//  ThunderDelivery
//
//  Created by david delgado on 9/1/15.
//  Copyright (c) 2015 david delgado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILoginViewModel.h"

@interface LoginView : UIView <ILoginViewModel>

@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *login;


@end
